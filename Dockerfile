#
# Docker container to Host lastest mono and vNext
#
# VERSION           0.3.1

FROM ubuntu:lucid
MAINTAINER Stefan Jutzi <stefan.jutzi@swisscom.com>

# Get prerequisites
RUN sudo apt-get -y update && sudo apt-get -y upgrade
RUN sudo apt-get -y install bash curl wget git-core build-essential autoconf libtool g++ libgdiplus libgtk2.0-0 gettext unzip  

# mono
WORKDIR /tmp
RUN git clone https://github.com/mono/mono.git
WORKDIR mono
RUN ./autogen.sh --prefix=/usr
RUN make get-monolite-latest
RUN make EXTERNAL_MCS=${PWD}/mcs/class/lib/monolite/gmcs.exe
RUN sudo make install
RUN mozroots --import --sync
WORKDIR /root
RUN rm -rf /tmp/mono

# vNext
ENV HOME /root
RUN /bin/bash -c "curl https://raw.githubusercontent.com/aspnet/Home/master/kvminstall.sh | sh"
RUN /bin/bash -c "source ~/.kre/kvm/kvm.sh && ! kvm upgrade && k --version"

# WebApp
WORKDIR /opt
RUN git clone https://github.com/Yantrio/vNext.Nancy
WORKDIR vNext.Nancy/src/vNextNancy
RUN /bin/bash -c "source ~/.kre/kvm/kvm.sh && kpm restore"
# application start command
EXPOSE 1234
CMD /bin/bash -c "source ~/.kre/kvm/kvm.sh && k run"