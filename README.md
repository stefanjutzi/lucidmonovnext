#Ubuntu Lucid with Mono and ASP.NET
Includes sample [Nancy-vNext](https://github.com/Yantrio/vNext.Nancy) web app from [Yantrio](https://github.com/Yantrio).

## Running the sample Nancy application
1. ```docker run -i -p 3000:1234 stefanjutzi/lucidmonovnext```  
    This will start the image and forward the port 1234 on the container to the host on port 3000. 
    * The -i switch (interactive mode) is needed for the application to keep running.
    * Press enter to quit
